Clone AR-js (https://github.com/jeromeetienne/AR.js.git)
    - Copy aframe/build/arframe-ar.js
    - Copy aframe/examples/vendor/aframe/build/aframe.min.js

Clone three.js (https://github.com/mrdoob/three.js)
    - Copy three.js/build/three.js
    - Copy three.js/examples/js/controls/OrbitControls.js

2018-10-17 TST


Clone awe.js (https://github.com/awe-media/awe.js)
    - Copy awe/examples/marker_ar/awe-jsartoolkit-dependencies.js
    - Copy awe/examples/marker_ar/awe.marker_ar.js
    - Copy awe/js/awe-v8.js
    - Copy awe/js/awe-loader.js
    - Copy awe/js/awe-standard.js
    - Copy awe/js/awe-standard-dependencies.j
    - Copy awe/js/plugins/awe-standard-object_clicked.js
    - Copy awe/js/plugins/awe-standard-object_clicked_or_focused.js
    - Copy awe/js/plugins/awe.rendering_effects.js
    - Copy awe/js/plugins/StereoEffect.js
    - Copy awe/js/plugins/VREffect.js